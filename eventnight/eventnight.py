from datetime import datetime

from redbot.core import commands
from redbot.core.config import Config

default_settings = {
    "GAMELIST": [],
    "POLLSTART": "0 0 * * 6",
    "POLLEND": "0 0 * * 4",
    "EVENTSTART": "0 15 * * 5",
    "EVENTEND": "0 0 * * 6",
}

class EventNight(commands.Cog):
    """Set up Reoccurring events while allowing the members to vote on which game to play"""

    def __init__(self, bot):
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=191587265896644609,
            force_registration=True
        )
        self.config.register_guild(**default_settings)

    @commands.group(autohelp=False)
    async def eventnight(self, ctx: commands.Context):
        """This cog runs the game night events"""
        pass

    @eventnight.command(name="settings")
    @commands.admin()
    async def settings_list(self, ctx: commands.Context):
        """Lists the settings"""
        setting_names = {
            "POLLSTART": "Poll start: ",
            "POLLEND": "Poll end: ",
            "EVENTSTART": "Event start: ",
            "EVENTEND": "Event end: "
        }
        guild = ctx.message.guild
        settings = await self.config.guild(guild).get_raw()
        msg = "```\n"
        for attr, name in setting_names.items():
            msg += name + str(settings[attr]) + "\n"
        msg += "```"
        await ctx.send(msg)

    @eventnight.command(name="set")
    @commands.admin()
    async def settings_set(self, ctx: commands.Context, *, argField: str):
        """Set a setting"""
        guild = ctx.message.guild
        settings = await self.config.guild(guild).get_raw()
        args = argField.split(" ")
        setting = args.pop(0)
        param = " ".join(args)
        if setting in settings:
            settings[setting] = param
            await self.config.guild(guild).set(settings)
            newsettings = await self.config.guild(guild).get_raw()
            await ctx.send("`" + setting + "` is now `" + newsettings[setting] + "`")
        else:
            await ctx.send("`" + setting + "` is not a valid setting")

    @eventnight.command(name="add")
    async def add(self, ctx: commands.Context, *, argField: str):
        """Adds a game to be considered to the game list"""
        guild = ctx.message.guild
        gamelist = await self.config.guild(guild).GAMELIST()
        if argField in gamelist:
            await ctx.send("Game already exists.")
        else:
            gamelist.append(argField)
            await self.config.guild(guild).GAMELIST.set(gamelist)
            await ctx.send("`" + argField + "` added to voting options")

    @eventnight.command(name="remove")
    async def remove(self, ctx: commands.Context, *, argField: str):
        """Removes a game to be considered from the game list"""
        guild = ctx.message.guild
        gamelist = await self.config.guild(guild).GAMELIST()
        if argField in gamelist:
            gamelist.pop(gamelist.index(argField))
            await self.config.guild(guild).GAMELIST.set(gamelist)
            await ctx.send("`" + argField + "` removed.")
        else:
            await ctx.send("`" + argField + "` is not in the game list.")

    @eventnight.command(name="list")
    async def list(self, ctx: commands.Context):
        """Prints a list of current games on the list"""
        guild = ctx.message.guild
        gamelist = await self.config.guild(guild).GAMELIST()
        games = "Current games: "
        for name in gamelist:
            games = games + name + ", "
        await ctx.send(games[:-2])
